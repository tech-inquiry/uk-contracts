# A utility for retrieving UK Contract Finder data in flattened CSV form,
# unflattening via the Open Contracting Data Standard 'flatten' tool, and then
# simplifying for export into Postgres.
#
# Note that, while the 'releases' and 'awards' fields are designed as arrays,
# they are, in every instance so far, singletons. We therefore flatten the
# 'release' members into the main data structure and then turn the first (and
# only) entry of 'awards' into an item called 'award' if it exists.
#
# Copyright (c) 2021 Jack Poulson <jack@techinquiry.org>
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
import csv
import glob
import json
import pandas as pd
import requests

UK_CONTRACTS_BASE_URL = 'https://www.contractsfinder.service.gov.uk/harvester/Notices/Data/CSV'


# 'flatten-tool' will ignore all but the last supplier out of a list (sometimes
# there are more than 30!) since the UK government often supplies them with
# identical 'id' fields. We therefore drop these useless identifiers in the
# staging process before calling 'flatten-tool'.
SUPPLIER_ID_REGEX = 'releases/0/awards/\d/suppliers/\d+/id'

DATA_DIRECTORY = 'data'
STAGE_DIRECTORY = 'data-to-be-unflattened'


def stage_csv(filename, filename_staged):
    df = pd.read_csv(filename)
    df_new = df.drop(
      df.columns[df.columns.str.contains(SUPPLIER_ID_REGEX)], axis=1)
    df_new.to_csv(filename_staged)


def stage_directory_csvs(directory, stage_directory=STAGE_DIRECTORY):
    filenames = glob.glob('{}/**.csv'.format(directory))
    for filename in filenames:
        components = filename.split('/')
        last_component = components[-1]
        output_filename = '{}/{}'.format(stage_directory, last_component)
        print('{}, {}, {}'.format(filename, last_component, output_filename))
        stage_csv(filename, output_filename)


def retrieve_day(year, month, day, data_prefix=DATA_DIRECTORY,
    staged_prefix=STAGE_DIRECTORY, verbose=True):
    '''Retrieves a CSV for a single day of UK procurement requests.'''
    url = '{}/{}/{}/{}'.format(UK_CONTRACTS_BASE_URL, year, month, day)
    filename = '{}/{}.{}.{}.csv'.format(data_prefix, year, month, day)
    filename_staged = '{}/{}.{}.{}.csv'.format(staged_prefix, year, month, day)
    if verbose:
        print('url: {}'.format(url))
    df = pd.read_csv(url)
    df.to_csv(filename)
    stage_csv(filename, filename_staged)


def build_cache(last_year, last_month, last_day, prefix=DATA_DIRECTORY,
    verbose=True):
    for year in range(last_year, 2014, -1):
        year_str = str(year)
        for month in range(12, 0, -1):
            if year == last_year and month > last_month:
                continue
            month_str = str(month).zfill(2)
            if month == 1:
                day_range = range(31, 0, -1)
            elif month == 2:
                is_leap_year = (year % 4 == 0)
                if is_leap_year:
                    day_range = range(29, 0, -1)
                else:
                    day_range = range(28, 0, -1)
            elif month == 3:
                day_range = range(31, 0, -1)
            elif month == 4:
                day_range = range(30, 0, -1)
            elif month == 5:
                day_range = range(31, 0, -1)
            elif month == 6:
                day_range = range(30, 0, -1)
            elif month == 7:
                day_range = range(31, 0, -1)
            elif month == 8:
                day_range = range(31, 0, -1)
            elif month == 9:
                day_range = range(30, 0, -1)
            elif month == 10:
                day_range = range(31, 0, -1)
            elif month == 11:
                day_range = range(30, 0, -1)
            elif month == 12:
                day_range = range(31, 0, -1)
            for day in day_range:
                if year == last_year and month == last_month and day > last_day:
                    continue
                day_str = str(day).zfill(2)
                try:
                    retrieve_day(year_str, month_str, day_str, prefix, verbose)
                except:
                    print('Could not retrieve for {}.{}.{}'.format(
                        year, month, day))


def convert_csv_to_json(filenames):
    for filename in filenames:
        basename = filename[:-4]
        json_filename = basename + '.json'
        df = pd.read_csv(filename)
        df.to_json(json_name)


def convert_all_csv_to_json():
    convert_csv_to_json(glob.glob('{}/**.csv'.format(DATA_DIRECTORY)))


def simplify_unflattened_json(data):
    KEYS_TO_DELETE = [
        '', 'Unnamed: 0', 'version', 'extensions', 'license',
        'publicationPolicy'
    ]
    for item in data:
        for key in KEYS_TO_DELETE:
            if key in item:
                del item[key]

        if 'publishedDate' in item:
            item['published_date'] = item['publishedDate']
            del item['publishedDate']

        if 'deadlineDate' in item:
            item['deadline_date'] = item['deadlineDate']
            del item['deadlineDate']

        if 'releases' in item:
            releases = item['releases']
            if len(releases):
                if len(releases) > 1:
                    print(releases)
                    print("Error: 'releases' was of length {}".format(
                        len(releases)))
                release = releases[0]
                #if 'ocid' in release:
                #    item['ocid'] = release['ocid']
                #if 'id' in release:
                #    item['id'] = release['id']
                if 'tender' in release:
                    item['tender'] = release['tender']
                if 'buyer' in release:
                    item['buyer'] = release['buyer']
                if 'awards' in release:
                    awards = release['awards']
                    if len(awards):
                        if len(awards) > 1:
                            print(awards)
                            print("Error: 'awards' was of length {}".format(
                                len(awards)))
                        item['award'] = awards[0]
            del item['releases']

        if 'publisher' not in item:
            item['publisher'] = None
        if 'tender' not in item:
            item['tender'] = None
        if 'buyer' not in item:
            item['buyer'] = None
        if 'award' not in item:
            item['award'] = None


def simplified_json_to_csv(json_filename, csv_filename):
    '''Converts a JSON representation of cases into a CSV.'''
    df = pd.read_json(json_filename)

    date_columns = ['published_date', 'deadline_date']
    for date_column in date_columns:
        df[date_column] = pd.to_datetime(df[date_column],
                                         infer_datetime_format=True,
                                         errors='coerce')

    df['publisher'] = df['publisher'].apply(lambda value: json.dumps(value))
    df['tender'] = df['tender'].apply(lambda value: json.dumps(value))
    df['buyer'] = df['buyer'].apply(lambda value: json.dumps(value))
    df['award'] = df['award'].apply(lambda value: json.dumps(value))

    # We must use a tab separator and avoid newlines or MySQL will truncate
    # all text inputs to 255 characters. This causes JSON parsing issues for
    # some of Google's "lobbyist_positions" fields.

    df = df.replace(r'\\r\\n', '; ', regex=True)
    df = df.replace(r'\\r', '; ', regex=True)
    df = df.replace(r'\\n', '; ', regex=True)

    df = df.replace(r'\r\n', '; ', regex=True)
    df = df.replace(r'\r', '; ', regex=True)
    df = df.replace(r'\n', '; ', regex=True)

    df = df.replace(r'\\', r'\\\\', regex=True)

    # Replace all sequences of the form '\...;' with ';' since PostgreSQL
    # refuses to parse JSON otherwise.
    df = df.replace(r'\\+;', r';', regex=True)

    duplicated = df[['uri']].duplicated(keep='first')
    num_dropped = sum(duplicated)
    print('Dropping {} redundant rows.'.format(num_dropped))
    df = df.drop(df[duplicated].index)

    df.to_csv(csv_filename,
              index=False,
              escapechar='\\',
              quotechar='"',
              doublequote=False,
              sep='\t',
              quoting=csv.QUOTE_ALL)


def process_unflattened_json(
    unflattened_json='uk_contracts_unflattened.json',
    simplified_json='uk_contracts.json',
    csv_filename='uk_contract_filing.csv'):
    '''Outputs a Postgres-importable CSV from unflattened contract JSON.'''
    data = json.load(open(unflattened_json))
    simplify_unflattened_json(data)
    with open(simplified_json, 'w') as outfile:
        json.dump(data, outfile, indent=1)
    simplified_json_to_csv(simplified_json, csv_filename)

